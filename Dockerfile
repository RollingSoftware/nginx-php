FROM debian:bullseye

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y openssl zip unzip vim
RUN apt-get install -y apt-transport-https lsb-release ca-certificates curl wget
RUN apt-get install -y libonig-dev
RUN apt-get install -y gnupg2

RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
RUN sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'

RUN wget https://nginx.org/keys/nginx_signing.key
RUN apt-key add nginx_signing.key
RUN sh -c 'echo "deb https://nginx.org/packages/debian/ $(lsb_release -sc) nginx" > /etc/apt/sources.list.d/nginx.list'
RUN sh -c 'echo "deb-src https://nginx.org/packages/debian/ $(lsb_release -sc) nginx" > /etc/apt/sources.list.d/nginx.list'

RUN apt-get update

RUN apt-get install -y php8.1-fpm
RUN apt-get install -y php8.1-mysql php8.1-xml php8.1-pdo php8.1-mbstring

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
RUN mkdir -p /run/php/

RUN apt-get install -y nginx

RUN apt-get install -y python3 python3-pip python3-setuptools
RUN python3 -m pip install --upgrade pip setuptools wheel
RUN pip3 install supervisor

RUN apt-get clean
RUN apt-get autoremove

COPY ./supervisord.conf /etc/supervisord.conf
COPY default /etc/nginx/sites-available/default
COPY ./start.sh /start.sh

EXPOSE 80

ENTRYPOINT [ "/start.sh" ]
